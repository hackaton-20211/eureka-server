# Eureka Server

Control de Backends Dockerizado

## *WHAT IS A SPRING BOOT*
*Java Spring Framework (**Spring Framework**) is a popular, open source, enterprise-level framework for creating standalone, production-grade applications that run on the Java Virtual Machine (**JVM**).*

*Java Spring Boot (**Spring Boot**) is a tool that makes developing web application and microservices with Spring Framework faster and easier through **three core capabilities:***

1. **Autoconfiguration**
2. **An opinionated approach to configuration**
3. **The ability to create standalone applications**

*These features work together to provide you with a tool that allows you to set up a Spring-based application with minimal configuration and setup.*

![Spring Boot](https://programaenlinea.net/wp-content/uploads/2019/07/java.spring.png)
