FROM openjdk:17
EXPOSE 8080
COPY target/EurekaManager-2.6.1.jar /EurekaManager-2.6.1.jar
ENTRYPOINT ["java","-jar","/EurekaManager-2.6.1.jar"]